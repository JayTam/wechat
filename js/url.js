var productType = "dev";
var version="v1_4_0";
if (productType == "dev") {
	var myurl = "https://wwwdev.dazhi100.com/wechatjs/";
	var doorurl = "https://wwwdev.dazhi100.com/dzyxjs/";
	var doorurl1 = "https://wwwdev.dazhi100.com/nxdzyxjs/";

	var d_url = "https://wwwdev.dazhi100.com/agentjs/";
	var snurl = "https://wwwdev.dazhi100.com/nxdzyxjs/";
	var messageurl = "https://wwwdev.dazhi100.com/dzyxjs/";

	var gateway = "https://gateway-api-dev.dazhi100.com/app-server/"+version;
}
if (productType == "qfe") {
	var myurl = "https://wwwqfe.dazhi100.com/wechatjs/";
	var doorurl = "https://wwwqfe.dazhi100.com/dzyxjs/";
	var doorurl1 = "https://wwwqfe.dazhi100.com/nxdzyxjs/";

	var d_url = "https://wwwqfe.dazhi100.com/agentjs/";
	var snurl = "https://wwwqfe.dazhi100.com/nxdzyxjs/";
	var messageurl = "https://wwwqfe.dazhi100.com/dzyxjs/";

	var gateway = "https://gateway-api-qfe.dazhi100.com/app-server/"+version;
}
if (productType == "ppe") {
	var myurl = "https://wwwppe.dazhi100.com/wechatjs/";
	var doorurl = "https://wwwppe.dazhi100.com/dzyxjs/";
	var doorurl1 = "https://wwwppe.dazhi100.com/nxdzyxjs/";

	var d_url = "https://wwwppe.dazhi100.com/agentjs/";
	var snurl = "https://wwwppe.dazhi100.com/nxdzyxjs/";
	var messageurl = "https://www.dazhi100.com/dzyxjs/";

	var gateway = "https://gateway-api-ppe.dazhi100.com/app-server/"+version;
}
if (productType == "prod") {
	var myurl = "https://www.dazhi100.com/wechatjs/";
	var doorurl = "https://www.dazhi100.com/dzyxjs/";
	var doorurl1 = "https://www.dazhi100.com/nxdzyxjs/";

	var d_url = "https://www.dazhi100.com/agentjs/";
	var snurl = "https://www.dazhi100.com/nxdzyxjs/";
	var messageurl = "https://www.dazhi100.com/dzyxjs/";

	var gateway = "https://gateway-api.dazhi100.com/app-server/"+version;
}

var mydate = new Date();
//修改微信的头部
function title(w) {
	var $body = $('body');
	document.title = w;

	var $iframe = $('<iframe src="/favicon.ico"></iframe>');
	$iframe.on('load', function () {
		setTimeout(function () {
			$iframe.off('load').remove();
		}, 0);
	}).appendTo($body);
}
//获取验证码
function getnum(obj, phone) {
	if ($(obj).html().replace(/\s/g, "") != "获取验证码") {
		return;
	}
	$(obj).html("获取中");
	$.post(myurl + "wechatLogin_sendCodeMsg.action", { phoneNum: phone }, function (data) {
		//console.log(data)
		if (data.s == "1") {
			var a = 59;
			$(obj).html(a + "s后可获取");
			$(obj).css({ "color": "#bbbbbb" });
			var tim = setInterval(function () {
				a--;
				if (a == "0") {
					$(obj).css({ "color": "#ff9800" });
					$(obj).html("获取验证码");
					clearInterval(tim);
				} else {
					$(obj).html(a + "s后可获取");
				}

			}, 1000)
		} else {
			mui.toast("获取验证码失败");
		}
	})


}

//权限
function jurisdiction(obj, type) {

	// var str = "";
	// if (obj.indexOf("6")) {
	// 	str += ",1,2";
	// }
	// if (obj.indexOf("7")) {
	// 	str += ",3";
	// }
	// if (obj.indexOf("4")) {
	// 	str += ",4,5";
	// }
	// if (obj.indexOf("5")) {
	// 	str += ",7";
	// }
	// if (obj.indexOf("8")) {
	// 	str += ",6";
	// }
	// if (type == 1) {
	// 	str += ",a,b";
	// }
	// localStorage.setItem("jurisdiction", str.substring(1, str.length));
}

//后退刷新
function pushHistory(url, m) {

	window.addEventListener("popstate", function (e) {
		if (m && m == 1) {
			removeit();
			localStorage.removeItem("xtcrid1");
			localStorage.removeItem("teacherCpids");
			localStorage.removeItem("yxtid");
			localStorage.removeItem("kqtids");
			localStorage.removeItem("addtname");
			localStorage.removeItem("tcpids");
			localStorage.removeItem("xtcrid1");
		}
		// alert("回退！");
		if (url == "1") {
			//			localStorage.clear();
			WeixinJSBridge.call('closeWindow');
			return;
		}
		//window.history.back();
		//在历史记录中后退,这就像用户点击浏览器的后退按钮一样。

		//window.history.go(-1);
		//你可以使用go()方法从当前会话的历史记录中加载页面（当前页面位置索引值为0，上一页就是-1，下一页为1）。
		//alert(000)
		location.href = url;
		//可以获取前一页面的URL地址的方法,并返回上一页。

	}, false);
	//localStorage.setItem("his",1);
	var state = {
		title: "",
		url: "#"
	};
	window.history.pushState(state, "", "#");
}

//获取url的参数

function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg);  //匹配目标参数
	if (r != null) return decodeURI(r[2]); return null; //返回参数值
}

function goback() {
	window.history.go(0);
}
//时间计算
function gettime(time, a) {
	var mydate = "";
	if (a == 0) {         //减30分钟
		var m = time.substring(3, 5);
		var h = time.substring(0, 2);
		if (m - 30 >= 0) {
			mydate = h + ":" + (m - 30);
		} else {
			if (h - 1 >= 0) {
				mydate = h - 1 + ":" + (60 + (m - 0) - 30);
			} else {
				mydate = "23:" + (60 + (m - 0) - 30);
			}
		}
	} else if (a == 1) {    //加30分钟
		var m = time.substring(3, 5);
		var h = time.substring(0, 2);
		if (m - 0 + 30 < 60) {
			mydate = h + ":" + (m - 0 + 30);
		} else {
			if (h - 0 + 1 < 24) {
				mydate = (h - 0 + 1) + ":" + (m - 0 + 30 - 60);
			} else {
				mydate = "00:" + (m - 0 + 30 - 60);
			}
		}
	}
	//	var list=mydate.split(":"),date1="",date2="";
	//	
	//	if(list[0]!="00" && list[0]-10<0){
	//		date1="0"+list[0];
	//	}else{
	//		date1=list[0];
	//	}
	//	console.log(date1)
	//	if(list[1]-10<0){
	//		date2="0"+list[1];
	//	}else{
	//		date2=list[1];
	//	}
	//	mydate=date1+":"+date2;
	return mydate;
}

//获取分钟数
function getm(time) {
	var m = Number(time.split(":")[0]) * 60 + Number(time.split(":")[1]);
	return m;
}
//获取分钟数的option
function getmop(time1, time2, a, b, c) {
	var title = "延后";
	if (a == 1) {
		title = "提前";
	}
	var m1 = Number(time1.split(":")[0]) * 60 + Number(time1.split(":")[1]);
	var m2 = Number(time2.split(":")[0]) * 60 + Number(time2.split(":")[1]);
	var s = m2 - m1;
	if (s >= 0) {
		if (c) {
			s = 1440 - m2 + m1;
		} else {
			s = s;
		}
	} else {
		if (c) {
			s = 1440 - m2 + m1;
		} else {
			s = Math.abs(s);
		}
	}
	if (s > 720) {
		s = 720;
		b = 2;
	}
	var date1 = s;
	var yu = date1 % 10, le = parseInt(date1 / 10);
	var opt = '<option value="请选择">请选择</option>';
	if (b == 1) {
		for (var i = 1; i < le; i++) {
			opt += '<option value="' + i * 10 + '">' + title + ' ' + i * 10 + ' 分钟</option>';
		}
	} else {
		for (var i = 1; i <= le; i++) {
			opt += '<option value="' + i * 10 + '">' + title + ' ' + i * 10 + ' 分钟</option>';
		}
	}
	if (yu > 0) {
		var maxm = (le) * 10 + yu;
		opt += '<option value="' + maxm + '">' + title + ' ' + maxm + ' 分钟</option>';
	}
	return opt;
}

//根据一个时间算另一个事件
function getOtherTime(time, minute, a) {
	var mydate = "";
	var m = Number(time.split(":")[0]) * 60 + Number(time.split(":")[1]);
	if (a == 1) {//提前
		if (m - minute < 0) {
			var m1 = m - minute + 1440;
			var h1 = parseInt(m1 / 60);
			if (h1 < 0) {
				h1 = 0;
			}
			var h = h1;
			var m2 = m1 % 60;
			if (h < 10) {
				h = "0" + h;
			}
			if (m2 < 10) {
				m2 = "0" + m2;
			}
			mydate = h + ":" + m2
		} else {
			var h = parseInt((m - minute) / 60);
			var m1 = (m - minute) % 60;
			if (h < 10) {
				h = "0" + h;
			}
			if (m1 < 10) {
				m1 = "0" + m1;
			}
			mydate = h + ":" + m1
		}
	} else {//延后
		var h = parseInt(minute / 60);
		var h1 = Number(time.split(":")[0]);
		var m = Number(time.split(":")[1]);
		if (h + h1 > 23) {
			var h2 = h + h1 - 24;
			var m1 = Number(time.split(":")[1]) + minute % 60;
			if (m1 > 59) {
				h2 += 1;
				m1 = m1 % 60;
			}
			if (h2 < 10) {
				h2 = "0" + h2;
			}
			if (m1 < 10) {
				m1 = "0" + m1;
			}
			mydate = h2 + ":" + m1;
		} else {
			var h2 = h + h1;
			var m1 = Number(time.split(":")[1]) + minute % 60;
			if (m1 > 59) {
				h2 += 1;
				m1 = m1 % 60;
			}
			if (h2 < 10) {
				h2 = "0" + h2;
			}
			if (m1 < 10) {
				m1 = "0" + m1;
			}
			mydate = h2 + ":" + m1;
		}

	}
	return mydate;
}
function removeit() {
	localStorage.removeItem("have");
	localStorage.removeItem("time1");
	localStorage.removeItem("time2");
	localStorage.removeItem("time3");
	localStorage.removeItem("time4");
	localStorage.removeItem("time5");
	localStorage.removeItem("time6");
	localStorage.removeItem("time7");
	localStorage.removeItem("time8");
	localStorage.removeItem("ttime1");
	localStorage.removeItem("ttime2");
	localStorage.removeItem("ttime3");
	localStorage.removeItem("ttime4");
	localStorage.removeItem("kname");
	localStorage.removeItem("xttime1");
	localStorage.removeItem("xttime2");
	localStorage.removeItem("xttime3");
	localStorage.removeItem("xttime4");
	localStorage.removeItem("c_tctime");
	localStorage.removeItem("c_tcpid");
	localStorage.removeItem("c_tname");
	localStorage.removeItem("tweek0");
	localStorage.removeItem("tweek1");
	localStorage.removeItem("tweek2");
	localStorage.removeItem("tweek3");
	localStorage.removeItem("tweek4");
	localStorage.removeItem("tweek5");
	localStorage.removeItem("tweek6");
}
//判断元素是否存在数组内
function contains(arr, obj) {
	var i = arr.length;
	while (i--) {
		if (arr[i] == obj) {
			return true;
		}
	}
	return false;
}
function getweek(time) {
	var str = "";
	var week = new Date(time).getDay();
	if (week == 0) {
		str = "星期日";
	} else if (week == 1) {
		str = "星期一";
	} else if (week == 2) {
		str = "星期二";
	} else if (week == 3) {
		str = "星期三";
	} else if (week == 4) {
		str = "星期四";
	} else if (week == 5) {
		str = "星期五";
	} else if (week == 6) {
		str = "星期六";
	}
	return str;
}
//修改迟到早退 时间变化
function chizao(r, w, checkPeriod) {
	var t = localStorage.getItem("timestr");
	if (r == 1) {//迟到
		var time = t.split(",");
		if (w == 1) {
			date1 = getOtherTime(checkPeriod.split("||")[0].split(",")[1], 1, 2);
			date2 = getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1);
			if (time[1]) {
				if (getm(date2) >= getm(time[1])) {
					date2 = getOtherTime(time[1], 1, 1);
				}
			}
		} else if (w == 3) {
			date1 = getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2);
			date2 = getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1);
			if (time[1]) {
				if (getm(date2) >= getm(time[3])) {
					date2 = getOtherTime(time[3], 1, 1);
				}
			}
		}
		$(".mytime").html(date1);
	} else if (r == 2) {//早退
		var time = t.split(",");
		if (w == 2) {
			date1 = getOtherTime(checkPeriod.split("||")[0].split(",")[1], 1, 2);
			if (getm(date1) <= getm(time[0])) {
				date1 = getOtherTime(time[0], 1, 2);
			}
			date2 = getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1);
		} else if (w == 4) {
			date1 = getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2);
			date2 = getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1);
			if (getm(date1) <= getm(time[2])) {
				date1 = getOtherTime(time[2], 1, 2);
			}
		}
		$(".mytime").html(date2);
	} else if (r == 0) {//正常
		if (w == 1) {
			date1 = getOtherTime(checkPeriod.split("||")[0].split(",")[1], checkPeriod.split("||")[0].split(",")[0], 1);
			date2 = checkPeriod.split("||")[0].split(",")[1];
			if (getm(date1) > getm(date2)) {
				date1 = "00:00";
			}
			$(".mytime").html(date2);
		} else if (w == 3) {
			date1 = getOtherTime(checkPeriod.split("||")[1].split(",")[1], checkPeriod.split("||")[1].split(",")[0], 1);
			date2 = checkPeriod.split("||")[1].split(",")[2];
			if (getm(date1) > getm(date2)) {
				date1 = "00:00";
			}
			$(".mytime").html(date2);
		} else if (w == 2) {
			date2 = getOtherTime(checkPeriod.split("||")[0].split(",")[2], checkPeriod.split("||")[0].split(",")[3], 2);
			date1 = checkPeriod.split("||")[0].split(",")[2];
			if (getm(date1) > getm(date2)) {
				date1 = "00:00";
			}
			$(".mytime").html(date1);
		} else if (w == 4) {
			date2 = getOtherTime(checkPeriod.split("||")[1].split(",")[2], checkPeriod.split("||")[1].split(",")[3], 2);
			date1 = checkPeriod.split("||")[1].split(",")[2];
			if (getm(date1) > getm(date2)) {
				date1 = "00:00";
			}
			$(".mytime").html(date1);
		}
	}
}
//判断是否可以修改为迟到或者早退
function lateOrEarly() {
	var checkPeriod = localStorage.getItem("checkPeriod");
	var work = localStorage.getItem("s_work");
	var t = localStorage.getItem("timestr").split(",");
	if (work == 1) {
		if (getm(t[0]) > getm(checkPeriod.split("||")[0].split(",")[1])) {
			if (t[1]) {
				if (getm(t[1]) > getm(checkPeriod.split("||")[0].split(",")[2])) {
					if (t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1) && t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[1], 1, 2)) {
						$(".chizao").css("display", "none");
					}
				} else {
					if (t[0] == getOtherTime(t[1], 1, 1) && t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[1], 1, 2)) {
						$(".chizao").css("display", "none");
					}
				}
			} else {
				if (t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1) && t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[1], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			}
		} else {
			if (t[1]) {
				if (getm(t[1]) > getm(checkPeriod.split("||")[0].split(",")[2])) {
					if (checkPeriod.split("||")[0].split(",")[1] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1)) {
						$(".chizao").css("display", "none");
					}
				} else {
					if (checkPeriod.split("||")[0].split(",")[1] == getOtherTime(t[1], 1, 1)) {
						$(".chizao").css("display", "none");
					}
				}
			} else {
				if (checkPeriod.split("||")[0].split(",")[1] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			}
		}

	} else if (work == 2) {
		if (getm(t[0]) > getm(checkPeriod.split("||")[0].split(",")[1])) {
			if (getm(t[1]) > getm(checkPeriod.split("||")[0].split(",")[2])) {
				if (t[0] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			} else {
				if (t[1] == getOtherTime(t[0], 1, 2) && t[1] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			}
		} else {
			if (getm(t[1]) > getm(checkPeriod.split("||")[0].split(",")[2])) {
				if (checkPeriod.split("||")[0].split(",")[1] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			} else {
				if (checkPeriod.split("||")[0].split(",")[1] == getOtherTime(t[1], 1, 1) && t[1] == getOtherTime(checkPeriod.split("||")[0].split(",")[2], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			}
		}
	} else if (work == 3) {
		if (getm(t[2]) > getm(checkPeriod.split("||")[1].split(",")[1])) {
			if (t[3]) {
				if (getm(t[3]) > getm(checkPeriod.split("||")[1].split(",")[2])) {
					if (t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1) && t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2)) {
						$(".chizao").css("display", "none");
					}
				} else {
					if (t[2] == getOtherTime(t[3], 1, 1) && t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2)) {
						$(".chizao").css("display", "none");
					}
				}
			} else {
				if (t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1) && t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			}
		} else {
			if (t[3]) {
				if (getm(t[3]) > getm(checkPeriod.split("||")[1].split(",")[2])) {
					if (checkPeriod.split("||")[1].split(",")[1] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1)) {
						$(".chizao").css("display", "none");
					}
				} else {
					if (checkPeriod.split("||")[1].split(",")[1] == getOtherTime(t[3], 1, 1)) {
						$(".chizao").css("display", "none");
					}
				}
			} else {
				if (checkPeriod.split("||")[1].split(",")[1] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			}
		}
	} else if (work == 4) {
		if (getm(t[3]) > getm(checkPeriod.split("||")[1].split(",")[2])) {
			if (getm(t[2]) > getm(checkPeriod.split("||")[1].split(",")[1])) {
				if (t[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1)) {
					$(".chizao").css("display", "none");
				}
			} else {
				if (checkPeriod.split("||")[1].split(",")[2] == getOtherTime(checkPeriod.split("||")[1].split(",")[1], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			}
		} else {
			if (getm(t[2]) > getm(checkPeriod.split("||")[1].split(",")[1])) {
				if (t[3] == getOtherTime(checkPeriod.split("||")[1].split(",")[2], 1, 1) && t[3] == getOtherTime(t[2], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			} else {
				if (checkPeriod.split("||")[1].split(",")[1] == getOtherTime(t[3], 1, 1) && checkPeriod.split("||")[1].split(",")[2] == getOtherTime(t[3], 1, 2)) {
					$(".chizao").css("display", "none");
				}
			}
		}
	}
}
function closeFuc() {
	$(".img_mask").hide();
}
//加载图片
function loadImgFuc(obj) {
	$("#img_div").html("");
	document.getElementById("img_div").style.setProperty("height", document.body.clientHeight + "px");
	document.getElementById("img_div").style.setProperty("line-height", document.body.clientHeight + "px");
	document.getElementById("img_div").style.setProperty("width", document.body.clientWidth + "px");
	$("#img_div").html('<p style="color:#fff;text-align: center;font-size: 12px;width: 100%;">图片加载中...</p>');
	$(".img_mask").show();
	var img = new Image();
	var imgurl = obj.attr("img");
	// img.src="http://picture-educloud.oss-cn-hangzhou.aliyuncs.com/iccardRecord/"+imgurl;
	img.src = imgurl;
	img.id = "bigImg";
	var yload = 0;
	img.onload = function () {
		img.width = document.body.clientWidth;
		img.style.setProperty("opacity", "0");
		$("#img_div").html(img);
		// console.log($("#bigImg").height())
		// , "margin-top": (document.body.clientHeight - $("#bigImg").height()) / 4 + "px"
		$("#bigImg").css({ "position": "absolute" })
		yload = 1;
		$("#bigImg").css("opacity", "1");
	}
	setTimeout(function () {
		if (yload == 0) {
			$("#img_div").html('<p style="color:#fff;text-align: center;font-size: 12px;width: 100%;text-align: center;vertical-align:middle">图片加载失败</p>');
		}
	}, 5000);
}