<div class="picker-panel">
<!--other box-->
<div class="box-day">
  <div class="check-line"></div>
  <div class="day-checked">
    <div class="day-list">
      <div class="list-div" v-for="day in renderListDay">
       {{day.value}}
      </div>
    </div>
  </div>
  <div class="day-wheel">
    <div class="wheel-div" v-for="day in renderListDay" transform: rotate3d(1, 0, 0, 80deg) translate3d(0px, 0px, 2.5rem);>
    {{day.value}}
    </div>
  </div>
</div>
<!--other box-->
</div>