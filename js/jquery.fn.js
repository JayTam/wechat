$.extend({
    /**
     *
     * @param type post or get
     * @param url 请求地址
     * @param json 数据格式
     * @param callback 请求成功返回函数
     * @constructor
     */
    Ajax: function (type, url, json, callback) {
        $.ajax({
            type: type,
            url: url,
            data: JSON.stringify(json),
            // data: $.param(json),
            contentType: 'application/json;charset=utf-8',
            // contentType: 'application/x-www-form-urlencoded;charset=utf-8',
            dataType: "json",
            crossDomain: true == !(document.all), //这句是关键
            xhrFields: { withCredentials: false },
            beforeSend: function (XMLHttpRequest) {
                if (localStorage.getItem("token")) {
                    XMLHttpRequest.setRequestHeader("token", localStorage.getItem("token"));
                }
                var str = "";
                if (localStorage.getItem("isadmin")!="undefined") {
                    str += "userType:"+localStorage.getItem("isadmin")+",";
                } else {
                    str += "userType:3,"
                }
                str += "schoolId:" + localStorage.getItem("sid");
                XMLHttpRequest.setRequestHeader("userInfo", str);
            },
            success: function (d) {
                if (d.responseCode == "501") {
                    localStorage.setItem("token", " ");
                    window.location.reload();
                }
                if (typeof callback == "function") {
                    callback(d);
                }
            },
            complete: function () {
                $(".load_div").remove();
            }
        });
    },
    //兼容老代码
    post: function (url, json, callback) {
        $.ajax({
            type: "post",
            url: url,
            data: $.param(json),
            contentType: 'application/x-www-form-urlencoded;charset=utf-8',
            dataType: "json",
            crossDomain: true == !(document.all), //这句是关键
            xhrFields: { withCredentials: false },
            beforeSend: function (XMLHttpRequest) {
                if (localStorage.getItem("token")) {
                    XMLHttpRequest.setRequestHeader("token", localStorage.getItem("token"));
                }
            },
            success: function (d) {
                if (d.responseCode == "501") {
                    localStorage.setItem("token", " ");

                    window.location.reload();
                }
                if (typeof callback == "function") {
                    callback(d);
                }
            },
            complete: function () {
                $(".load_div").remove();
            }
        });
    },
    get: function (url, json, callback) {
        $.ajax({
            type: "get",
            url: url,
            data: $.param(json),
            contentType: 'application/x-www-form-urlencoded;charset=utf-8',
            dataType: "json",
            crossDomain: true == !(document.all), //这句是关键
            xhrFields: { withCredentials: false },
            beforeSend: function (XMLHttpRequest) {
                if (localStorage.getItem("token")) {
                    XMLHttpRequest.setRequestHeader("token", localStorage.getItem("token"));
                }
                var str = "";
                if (localStorage.getItem("isadmin")!="undefined") {
                    str += "userType:"+localStorage.getItem("isadmin")+",";
                } else {
                    str += "userType:3,"
                }
                str += "schoolId:" + localStorage.getItem("sid");
                XMLHttpRequest.setRequestHeader("userInfo", str);

            },
            success: function (d) {
                if (d.responseCode == "501") {
                    localStorage.setItem("token", " ");

                    window.location.reload();
                }
                if (typeof callback == "function") {
                    callback(d);
                }
            },
            complete: function () {
                $(".load_div").remove();
            }
        });
    }
});
