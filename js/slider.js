
$.fn.Slider=function(e){
	$(this).unbind();
	var obj=$(this);
	var sliderTrack=$(obj).prev();
	var sliderInner=$(obj).parent();
	var num=$(sliderInner).next();
	var w,m,f;
	f=e.f;
		if(e.s){
			//console.log(e.s)
			if(e.s==1){
				w=e.w;
				$(sliderTrack).css({"width":"0%"});
				$(obj).css({"margin-left":"0%"});
				$(num).html("1");
			}else if(e.s==9){
				w=e.w;
				$(sliderTrack).css({"width":"100%"});
				$(obj).css({"margin-left":"100%"});
				$(num).html("9");
			}else{
				w=e.w;
				$(sliderTrack).css({"width":w+"%"});
				$(obj).css({"margin-left":w+"%"});
				$(num).html(Math.round(e.w/100*e.m));
			}
			
		}else{
			w=e.w;
			$(sliderTrack).css({"width":w+"%"});
			$(obj).css({"margin-left":w+"%"});
			$(num).html(Math.round(e.w/100*e.m));
		}
	var o=$(obj).attr("id");
	var tkw=$(sliderTrack).outerWidth();
	var inw=$(sliderInner).outerWidth();
	var first="",tkw="",inw="",fiy="";
	var mynum=$(num).html();
	document.getElementById(o).addEventListener("touchstart", function(event) {
	    	 var event = event || window.event;  
	    	 first=event.touches[0].clientX;
	    	 fiy=event.touches[0].clientY;
	    	 tkw=$(sliderTrack).outerWidth();
	 	 	 inw=$(sliderInner).outerWidth();	
	 	 	 $("body").on("touchmove",function(event){
				event.preventDefault; 
			}, false)
	},false);  
	document.getElementById(o).addEventListener("touchmove", function(event) {
	    	 var event = event || window.event;  
	    	 var last=event.touches[event.touches.length-1].clientX;
	    	 var lasty=event.touches[event.touches.length-1].clientY;
	    	 var b=Math.round((last-first+tkw)/inw*100);
	    	//console.log(lasty)
	    	 if(fiy-lasty>20 || fiy-lasty<-20){
	    	 	return;
	    	 }
	    	  if(e.s){
	    	  	if(Math.round(b/100*e.m)==0){
	    	  		$(num).html("1");
	    	  	}else{
	    	  		if(Math.round(b/100*e.m)==10){
	    	  			$(num).html("9");
	    	  		}else{
	    	  			$(num).html(Math.round(b/100*e.m));
	    	  		}
	    	  		
	    	  	}
				
			 }else{
				$(num).html(Math.round(b/100*e.m))
			 }
	    	 
	    	 if(b<96 && b>4){
	    	 	 $(sliderTrack).css({"width":b+"%"},1);
				 $(obj).css({"margin-left":b+"%"},1);
	    	 }
	    	 if(b<5){
	    	 	 $(sliderTrack).animate({"width":"0px"},1);
				 $(obj).animate({"margin-left":"0px"},1);
				 if(e.s){
				 	$(num).html("1");
				 }else{
				 	$(num).html("0");
				 }
	    	 }
	    	 if(b>95){
	    	 	 $(sliderTrack).animate({"width":"100%"},1);
				 $(obj).animate({"margin-left":"100%"},1);
				 if(e.s){
				 	$(num).html(e.m);
				 }else{
				 	$(num).html(e.m);
				 }
	    	 }
	    		
	},false);  
	document.getElementById(o).addEventListener("touchend", function(event) {
			$("body").off("touchmove");
	    	f(mynum);
	    	
	},false); 
}



