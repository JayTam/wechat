/**
 * Created by jyhuang on 2017/8/31.
 */
var rclass = /[\t\r\n\f]/g;
jQuery.fn.extend({
    //存在class
    hasClass: function(selector) {
        var className = " " + selector + " ",
            i = 0,
            l = this.length;
        for (; i < l; i++) {
            if (this[i].nodeType === 1 &&
                (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) > -1) {
                return true;
            }
        }
        return false;
    },
    //提示
    tip:function(le){
            $(this).mouseover(function(){
                if($(this).attr("tip").length>le){
                    var x=$(this).offset().top;
                    var y=$(this).offset().left;
                    var w=$(this).width();
                    var h=$(this).height();
                    var content=$(this).attr("tip");
                    var html="<div class='swrap' style='padding:5px; position: absolute;top:"+(x+h-5)+"px; left:"+(y+w)+"px;color:#2f9833;border-radius:4px;background-color:#eeeeee;font-size:10px;border:1px solid #2f9833;'>"+content+"</div>";
                    $("body").append(html);
                }

            });
            $(this).mouseout(function(){

                $(".swrap").remove();


            });

    },
    //加载页面
    pageLoad:function(url,callback){
        var _this=$(this);
        var index=null;
        setTimeout(function(){
            $.ajax({
                url:url,
                type:'get',
                dataType:'html',
                data:{},
                beforeSend:function(){
                    _this.html(" ");
                    //$.MyCommon.PageLoading({ "loadingTips":"页面加载中..."});
                },
                error: function(){ _this.html("加载失败");},
                success:function(data){
                    _this.html(data);
                    //$.MyCommon.end();
                }
            });
        },100);

    }
});
var transferData;
$.extend({
    transferData:function(option){
        transferData=option;
    },
    getTransferData:function(){
        var data=transferData;
            return data;
    },
    delTransferData:function(){
        transferData=null;
    },
    getNowFormatDate:function(){
    	 var date = new Date();
    	    var seperator1 = "-";
    	    var month = date.getMonth() + 1;
    	    var strDate = date.getDate();
    	    if (month >= 1 && month <= 9) {
    	        month = "0" + month;
    	    }
    	    if (strDate >= 0 && strDate <= 9) {
    	        strDate = "0" + strDate;
    	    }
    	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    	    return currentdate;
    },
    getPreMonth:function(date){
    	var arr = date.split('-');
        var year = arr[0]; //获取当前日期的年份
        var month = arr[1]; //获取当前日期的月份
        var day = arr[2]; //获取当前日期的日
        var days = new Date(year, month, 0);
        days = days.getDate(); //获取当前日期中月的天数
        var year2 = year;
        var month2 = parseInt(month) - 1;
        if (month2 == 0) {
            year2 = parseInt(year2) - 1;
            month2 = 12;
        }
        var day2 = day;
        var days2 = new Date(year2, month2, 0);
        days2 = days2.getDate();
        if (day2 > days2) {
            day2 = days2;
        }
        if (month2 < 10) {
            month2 = '0' + month2;
        }
        var t2 = year2 + '-' + month2 + '-' + day2;
        return t2;
    },
    getNextMonth:function(date) {  
        var arr = date.split('-');  
        var year = arr[0]; //获取当前日期的年份  
        var month = arr[1]; //获取当前日期的月份  
        var day = arr[2]; //获取当前日期的日  
        var days = new Date(year, month, 0);  
        days = days.getDate(); //获取当前日期中的月的天数  
        var year2 = year;  
        var month2 = parseInt(month) + 1;  
        if (month2 == 13) {  
            year2 = parseInt(year2) + 1;  
            month2 = 1;  
        }  
        var day2 = day;  
        var days2 = new Date(year2, month2, 0);  
        days2 = days2.getDate();  
        if (day2 > days2) {  
            day2 = days2;  
        }  
        if (month2 < 10) {  
            month2 = '0' + month2;  
        }  
      
        var t2 = year2 + '-' + month2 + '-' + day2;  
        return t2;  
    }  
});






Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};


Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
//数组包含元素
Array.prototype.contains = function ( needle ) {
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
};
//日期格式化
Date.prototype.Format = function (fmt) { //author: meizz
	  var o = {
	    "M+": this.getMonth() + 1, //月份
	    "d+": this.getDate(), //日
	    "h+": this.getHours(), //小时
	    "m+": this.getMinutes(), //分
	    "s+": this.getSeconds(), //秒
	    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
	    "S": this.getMilliseconds() //毫秒
	  };
	  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	  for (var k in o)
	  if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	  return fmt;
	}


