var Url="https://www.dazhi100.com/agentjs/";
/*var Url="http://192.168.7.130:9090/CloudIntel/agentjs/";*/
var aid=localStorage.getItem("aid");
var app=angular.module('dzyx_service', ['ionic']);

    app.run(function($ionicPlatform,$location, $rootScope, $stateParams) {
        $ionicPlatform.ready(function() {

            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                //alert(toState.name);
            })

    });
    //路由
    app.config(function($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('/',{url:'/',templateUrl:'main.html'})
            .state('/form',{url:'/form',templateUrl:'form.html'})
            .state('/List',{url:'/List',templateUrl:'List.html'})
            .state('/detail',{url:'/detail',templateUrl:'detail.html'})
            .state('/gdDetail',{url:'/GdDetail',templateUrl:'GdDetail.html'})
            .state('/parts',{url:'/parts',templateUrl:'parts.html'})
            .state('/seeorder',{url:'/seeorder',templateUrl:'seeorder.html'})
        $urlRouterProvider.otherwise('/');
    });

