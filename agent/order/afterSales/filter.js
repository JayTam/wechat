app.filter("orderState",[function(){
    return function(text){
        if(text==0){
            return "待上传凭证";
        }else if(text==1){
            return "待审核";
        }else if(text==2){
            return "审核未通过";
        }else if(text==3){
            return "待发货";
        }else if(text==4){
            return "待收货";
        }
    }
}]);
app.filter("pinfo",[function(){
    return function(text){
        if(text==0){
            return"不开发票";
        }else if(text==1){
            return"增值税专用10%";
        }else if(text==2){
            return"普通服务费2%";
        }else if(text==3){
            return"普通门禁5%";
        }
    }
}])
app.filter("zt",[function(){
    return function(text){
        if(text==""){
            return"自提";
        }else{
            return text;
        }
    }
}]);
