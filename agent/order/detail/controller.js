app .controller('detailCrl',['$scope','$timeout' ,'$http','$ionicLoading','$ionicModal',function($scope,$timeout,$http,$ionicLoading,$ionicModal){

        $http({
            method: 'POST',
            url: Url+"order_findWechatOrderDetail.shtml",
            params:{"aid":aid,"orderNum":localStorage.getItem("orderNum"),"state":localStorage.getItem("state")}
        }).then(function successCallback(response) {
            $scope.deviceInfo=response.data.deviceInfo;
            $scope.invoiceName=response.data.invoiceName;
            $scope.detailInfo=response.data.agentOrder;
            var dataArr=[];
            var allprice=0;
            for(var i=0;i<$scope.detailInfo.model.split("#").length;i++){
                var o={};
                for(var j=0;j<$scope.detailInfo.model.split("#")[i].split(",").length;j++){
                        o.img=$scope.detailInfo.model.split("#")[i].split(",")[0];
                        o.pname=$scope.detailInfo.model.split("#")[i].split(",")[1];
                        o.color=$scope.detailInfo.model.split("#")[i].split(",")[2];
                        o.aprice=$scope.detailInfo.model.split("#")[i].split(",")[3];
                        o.price=$scope.detailInfo.model.split("#")[i].split(",")[6];
                        o.count=$scope.detailInfo.model.split("#")[i].split(",")[4];
                        o.modeldzf=$scope.detailInfo.model.split("#")[i].split(",")[5];
                }
                allprice=allprice+((Number(o.price)*Number(o.count))+Number(o.modeldzf));
                dataArr.push(o);
            }
			 $scope.allprice=allprice.toFixed(2);
            $scope.choseCartP=dataArr;
        }, function errorCallback(response) {
            // 请求失败执行代码
        });

        $scope.uploadImgs=function(orderId){
        $scope.choseorderId=orderId;
        location.href="../upload/index.html#/uploadimg/"+orderId;

    };

        $scope.delOrder=function(orderId){
        var layerM=layer.confirm('确定删除订单号为'+orderId+'？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $ionicLoading.show({
                content: 'Loading',
                animation: 'slide-in-up',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            $.post(Url+"order_deleteOrder.shtml",{"orderNum":orderId},function(data){
                layer.close(layerM);
                if(data.s==1){
                    toastr.success("取消订单成功");

                    setTimeout(function(){
                        history.back();
                    },1000);

                }else{
                    toastr.error("取消订单失败");
                }
            })
        }, function(){

        });
    }

        $scope.seeImgs=function(orderId){
        location.href="../upload/index.html#/seeimg/"+orderId;
        }
        $scope.seewl=function(orderId,seid){
            location.href="../upload/index.html#/seewl/"+orderId+","+seid;
        }
        $scope.sure=function(orderId){
            var layerM=layer.confirm('确定收到订单号为'+orderId+'产品？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'slide-in-up',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                $.post(Url+"order_confirmOrder.shtml",{"orderNum":orderId},function(data){
                    if(data.s==1){
                        toastr.success("确认收货成功");
                        $ionicLoading.hide();
                        setTimeout(function(){
                            /*location.reload();*/
                            history.back();
                        },1000);
                    }else{
                        toastr.error("确认收货失败");
                    }
                });
            }, function(){

            });
        }

}]);