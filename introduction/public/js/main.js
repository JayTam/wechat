/**
 * Created by jyhuang on 2017/4/17.
 */
require.config({
    baseUrl: "public/js",
    paths: {
        "jquery": "jquery/jquery-3.1.1.min",
        "lazyload": "lazyload/jquery.lazyload",
        "bootstrap":"bootstrap/bootstrap.min",
        "bootstrap-datatimepicker":"bootstrap-data/bootstrap-datetimepicker.min",
        "bootstrap-datatimepicker-fr":"bootstrap-data/locales/bootstrap-datetimepicker.fr",
        "dataPlug":"dataPlug",
        "exif":"exif",
        "Uploadimgs":"Uploadimgs",
        "swiper":"swiper/swiper.min",
        "TweenMax":"greensock/TweenMax",
        "jquery.extend":"jquery/jquery.extend",
        "jssor":"jssor/jssor",
        "waterfall":"waterfall/jquery.waterfall",
        "mobile":"mobile",
        "layer":"layer/layer",
        /*"layerpage":"layerpage/laypage"*/
        "z-page":"fpage/js/jquery.z-pager",
        "pull":"pull/dropload.min"
    },
    shim: {
        "underscore" : {
            exports : "_"
        },
        'lazyload': {
            deps: [ 'jquery']

        },
        'bootstrap':{
            deps:['jquery']
        },
        'bootstrap-datatimepicker-fr':{
            deps:['jquery','bootstrap','bootstrap-datatimepicker']
        },
        'dataPlug':{
            deps:['jquery','bootstrap','bootstrap-datatimepicker','bootstrap-datatimepicker-fr']
        },
        'Uploadimgs':{
            deps:['jquery']
        },
        'swiper':{
            deps:['jquery']
        },
        'TweenMax':{
            deps:['jquery']
        },
        'jquery.extend':{
            deps:['jquery']
        },
        'jssor':{
            deps:['jquery']
        },
        'waterfall':{
            deps:['jquery']
        },
        'mobile':{
            deps:['jquery']
        },
        'layer':{
            deps:['jquery']
        },
        /*'layerpage':{
            deps:['jquery']
        }*/
        'z-page':{
            deps:['jquery']
        },
        'pull':{
            deps:['jquery']
        }
    }

});

require(['jquery','lazyload','bootstrap','bootstrap-datatimepicker',"dataPlug","Uploadimgs","TweenMax","jquery.extend","swiper","jssor","waterfall","mobile","layer","pull"], function (){
    $(function(){
        Init();
    });
});
