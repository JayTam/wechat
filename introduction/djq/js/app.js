  /**
 * Created by jyhuang on 2017/1/2.
 */
   var App={
   	/*url:"http://192.168.2.253:8090/CloudIntel/",*/
   	// url:"http://192.168.2.100:8080/GoodClass/",
   	url:"http://192.168.2.163:9090/CloudIntel/",
		//url:'http://192.168.2.163:9090/GoodClass/',
// 	/*url:"http://192.168.2.156:8080/GoodClass/",*/
   	//url:"http://www.dazhi100.com/",
   	type:function () {
            var u = navigator.userAgent;
            if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
                return "android";
            } else if (u.indexOf('iPhone') > -1) {//苹果手机
                return "ios";
            } else if (u.indexOf('Windows Phone') > -1) {//winphone手机
                return "winphone";
            }
    },
    App_screenX:function(){
        return document.body.clientWidth;
    },
    App_screenY:function(){
        return document.body.clientHeight;
    },
    set_standard:function(standardX,standardY){
        
            $("html").css({"font-size":20/(standardX/App.App_screenX())+"px"});
            $("html").attr("bz",20/(standardX/App.App_screenX()));
    },
    //开始点击
    touchstart:function(dom,callback){
    	 touch.on(dom, 'touchstart', function (ev) {
            ev.preventDefault();
            if(typeof callback=="function"){
           	  	callback();
           		}
            });
    },
    //点击
    tap:function(dom,callback){
    	touch.on(dom, 'tap', function (ev) {
    		ev.preventDefault();
               if(typeof callback=="function"){
           	  	callback();
           		}
            });
    	
    },
    //双击
    doubletap:function(dom,callback){
    	touch.on(dom, 'doubletap', function (ev) {
               if(typeof callback=="function"){
           	  	callback(ev);
           		}
            });
    },
    //解除事件绑定
    gestureoff:function(dom,type,callback){
    	touch.off( dom,type,callback );
    },
    // 手机震动
    deviceVibrate:function() {
	    var str = "";
	    switch ( plus.os.name ) {
	    	case "iOS":
	            if ( plus.device.model.indexOf("iPhone") >= 0 ) {
	                plus.device.vibrate();
	                str += "设备振动中...";
	            } else {
	                str += "此设备不支持振动";
	            }
	    	break;
	    	default:
	    		plus.device.vibrate();
	            str += "设备振动中...";
	    	break;
	    }
		return str;
	},
	//声音
    deviceBeep:function() {
	    
		s = plus.audio.createPlayer( "img/voice.mp3" );
		
	    var num = s.getDuration();//获取音频总长度number
	    
	    setTimeout(function(){//延时获取，否则可能没有返回长度
	        var num = s.getDuration();
	        
	    },100)
	 
	    s.play( function () {
	        
	    }, function ( e ) {
	        
	    } );  




	},
	//拨打电话
	dial :function(num){
		plus.device.dial(num,false);
	},
	//本地存储信息
	setItemFun:function(key,value){
		plus.storage.setItem( key, value );
	},
	//
	pushItemFuc:function(key,value){
		
	},
	//获得存储信息
	getItem:function(key){
	  	var value = plus.storage.getItem(key);
	  	return value;
	},
	//删除存储信息
	delItem:function(key){
		plus.storage.removeItem(key);
	},
	//创建推送消息
	createLocalPushMsg:function(str){
		var options = {cover:false};
		plus.push.createMessage( str, "LocalMSG", options );
	},
	animateWindow:function(url,type){
		var wp;
		wp||(wp=plus.webview.create(url,url,{scrollIndicator:'none',scalable:false,popGesture:'none'},{preate:true}));
		wp.show(type);
	},
	printInfo:function(str){
		return JSON.stringify(str);
	},
	
	Complete:function(){
            	document.onreadystatechange = subSomething;//当页面加载状态改变的时候执行这个方法. 
								function subSomething() 
								{ 
								if(document.readyState == "complete") //当页面加载状态 
									$(".app_show").fadeIn();
									
								} 
 },
	
	
	gesture:function (dom,dom2) {
		
            touch.on(dom, 'touchstart', function (ev) {
                //ev.startRotate();
                ev.preventDefault();
            });
            var dx, dy; 
            var angle = 0;
            var initialScale = 1;
            var currentScale;
            touch.on(dom, 'drag', function (ev) {
                dx = dx || 0;
                dy = dy || 0;
            
                var offx = dx + (ev.x);
                var offy = dy + ev.y;
                
                if(isNaN(offx)){
                	
                }else{
                	TweenMax.set(dom2, {x: offx, y: offy});
                }
                
                
                
            });

            touch.on(dom, 'dragend', function (ev) {
                dx += ev.x;
                dy += ev.y;
            });
            /*touch.on(dom, 'rotate', function (ev) {
                var totalAngle = angle + ev.rotation;
                if (ev.fingerStatus === 'end') {
                    angle = angle + ev.rotation;
                }
                TweenMax.set(dom2, {rotation: angle});
            });*/
            touch.on(dom, 'pinchend', function (ev) {
                currentScale = ev.scale - 1;
                currentScale = initialScale + currentScale;
                console.log(currentScale);
                currentScale = currentScale > 2 ? 2 : currentScale;
                 currentScale = currentScale < 0.5 ? 0.5 : currentScale;
                TweenMax.set(dom2, {scale: currentScale});
               
            });

            touch.on(dom, 'pinchend', function (ev) {
                initialScale = currentScale;
            });
            
        },
        
        
        getNowFormatDate:function () {
		    var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
		    return currentdate;
		}
}