<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		 <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    	<title>大智云校</title>
		<link rel="stylesheet" type="text/css" href="../public/css/bootstrap/bootstrap.min.css"/>
		<script type="text/javascript" src="../public/js/jquery/jquery-3.1.1.min.js"></script>
		<script src="../public/js/bootstrap/bootstrap.min.js"></script>
		<script type="text/javascript" src="../public/js/app.js"></script>
		<script src="../public/js/pull/dropload.min.js"></script>
		<script type="text/javascript" src="../public/js/mui.min.js"></script>
		<link rel="stylesheet" href="../public/js/pull/dropload.css">
		<link rel="stylesheet" type="text/css" href="../public/css/mui.min.css"/>
		<script type="text/javascript" src="js/layer-v3.0.3/layer/mobile/layer.js"></script>
	</head>
	<style type="text/css">
		body{
			font-family: "微软雅黑";
			font-size: 1.5rem;
		}
		.qiye{
			font-size: 2rem;
			margin-left: 10px;
			font-weight: 600;
		}
		.content{
			margin: 0px 10px;
		}
		.content span{
			color:#dbdbdb;
		}
		.content .dazhi{
			margin-left: 20px;
			color:  #007AFF;
		}
		.content p{
			color: #686868;
		}
		.content img{
			margin-top: 20px;
		}
		
	</style>
	<body>
		<!--登录-->
		<div id="den" style="display: none;">
			<div style="width: 100%;margin-top: 1rem !important;"><input id="kahao" type="text" style="height:40px;border:1px solid #D9D9D9;border-radius:6px;width: 80%;margin-left: 10%;font-size: 1.5rem;" placeholder="请输入卡号"></div>
			<div style="width: 100%;margin: 10px 0px;"><input id="name1" type="text" style="height:40px;border:1px solid #D9D9D9;border-radius:6px;width: 80%;margin-left: 10%;font-size: 1.5rem;" placeholder="请输入宝宝或老师姓名"></div>
			<p style="margin-top: 0.2rem !important;margin-left: 10% !important;font-size: 1.2rem;color:#FFA60A;">确保IC卡号与宝宝或老师姓名一致</p>
			<div style="width: 100%;margin-top: 2rem !important;"><div id="ic_queding"   style="height:40px;border:1px solid #D9D9D9;border-radius:6px;width: 80%;margin-left: 10%!important;text-align:center;line-height:40px;background-color: #FFA60A;color: #fff;">确定</div></div>
		</div>
		
		
		
		<!--记录-->
		<div id="ji" style="display: none;">
			<div style="background-color:#FFA60A; text-align: center;height: 40px;line-height: 40px;" >
					<span id="title" style="position:static;color: #FFFFFF;text-align: center;width: 100%;" class="titlepostion">打卡记录</span>
					<span id='pickDateBtn' type="button" class="mui-btn" style="display:inline-block;position: absolute;right: 1rem;top:10px;width: 20px;height: 20px;background: url(img/rili.png) 0rem 0rem no-repeat;background-size: 20px 20px;border: 0px;"></span>
			</div>
			<div id="tim" style="text-align: center;margin-top: 10px!important;">
				
			</div>
			<div id="y" style="margin-top: 10px">
				<div id="yi" style="margin-top: 0rem!important;">
				</div>
			</div>
		</div>
		
	</body>
	<script>
		var da;
			var datetime = new Date();  
	       var year=datetime.getFullYear();//获取完整的年份(4位,1970)  
	       var month=datetime.getMonth()+1;//获取月份(0-11,0代表1月,用的时候记得加上1)  
	       if(month<=9){  
	           month="0"+month;  
	       }  
	       var date=datetime.getDate();//获取日(1-31)  
	       if(date<=9){  
	           date="0"+date;  
	       }  
	       var dateformat=year+"-"+month+"-"+date;  
	       
	        function  DateDiff(date1,date2){
			var time1 = (new Date(date1)).getTime();
			var time2 = (new Date(date2)).getTime();
			return Math.abs(time1-time2) / 1000 / 60 / 60 / 24;
		} 
		
		
		$(function(){
			$.post('oa.dazhi100.com/CloudOA/pubjs/icRecord_searchIcRecore',{date:dateformat,openId:${openId}},function(data){
				console.log(data)
	      				if(data.ic==1){
	      					$("#den").css({"display":""});
	      					$("#ic_queding").click(function (){
					      		if($("#kahao").val().length<1){
					      			mui.toast('请输入卡号', '');
					      		}else if($("#name1").val().length<1){
					      			mui.toast('请输入宝宝或老师姓名', '');
					      		}else{
					      			var icname=$("#kahao").val();
					      			var studentName=$("#name1").val();
					      			$.post('oa.dazhi100.com/CloudOA/pubjs/icRecord_boundIcCard',{icname:icname,username:studentName,openId:${openId}},function(data){
					      				if(data.s==1){
					      					$("#den").css({"display":"none"});
					      					tim(dateformat)
					      				}else{
					      					mui.toast("卡号或姓名错误")
					      				}
					      			},'json')
						      		
						      	}
				      		});
	      				}else{
	      					$("#ji").css({"display":""});
	      					if(data.listIcardInfo.length==0){
	      						mui.toast("暂无打卡记录")
	      					}else{
	      						$("#tim").html(data.listIcardInfo[0].cdate.substring(0,10))
	      						var list=data.listIcardInfo;
	      						var span="";
	      						for(var s=0;s<list.length;s++){
	      							var jinchu;
									var img;
									if(list[s].type=="1"){
										jinchu="img/jin.png";
									}else if(list[s].type=="2"){
										jinchu="img/chu.png"
									}
									if(!list[s].img){
												img="";
											}else{
												img="img/tu.png"
											}
	      							span='<div style="padding:0px 10px!important;width: 100%;height:3rem;line-height:3rem;font-size: 1rem;background-color: #fff;border-bottom: 1px solid #d9d9d9;">'+
															'<span style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.2rem!important;background: url(img/fuka.png) 0rem 0rem no-repeat;background-size: 1.2rem 0.91rem;vertical-align: middle;"></span>'+
															'<span style="display: inline-block;height: 3rem;line-height: 3rem;text-indent: 0;width:50%;text-align:left;">'+list[s].icname+'</span>'+
															'<span style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.5rem!important;background: url('+jinchu+') 0rem 0rem no-repeat;background-size: 1.2rem 1.2rem;vertical-align: middle;"></span>'+
															'<span style="display: inline-block;height: 3rem;line-height: 3rem;text-indent: 0;margin: 0rem 1rem!important;">'+list[s].cdate.substring(11,19)+'</span>'+
															'<span class="datus" scs='+list[s].img+' style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.5rem!important;background: url('+img+') 0rem 0rem no-repeat;background-size: 1.4rem 1.225rem;vertical-align: middle;"></span>'+
														'</div>'+span	
	      						}
	      						$("#yi").html(span);
	      						$("#yi").on("click",".datus",function(){
	      							layer.open({
										  type: 1,
										  skin: 'layui-layer-rim', //加上边框
										  area: ['100%', '100%'], //宽高
										  content: '<img src='+$(this).attr("scs")+' width="100%" alt="" />'
										});
	      						})
	      					}
	      				}
	      			},'json')
		})
		
		
		mui.plusReady(function(){
			//选择日期
      	document.getElementById("pickDateBtn").addEventListener('tap', function() {
		    var dDate = new Date();
		    //设置当前日期（不设置默认当前日期）
		    //dDate.setFullYear(2016, 7, 16);
		    var minDate = new Date();
		    //最小时间
		    minDate.setFullYear(2000, 0, 1);
		    var maxDate = new Date();
		    //最大时间
		    maxDate.setFullYear(year, month-1, date);
		    plus.nativeUI.pickDate(function(e) {
		        var d = e.date;
		        var yue,ri;
		        //mui.toast('您选择的日期是:' + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
		        if((d.getMonth() + 1)<10){
		        	yue="0"+(d.getMonth() + 1);
		        }else{
		        	yue=d.getMonth() + 1;
		        }
		        if(d.getDate()<10){
		        	ri="0"+d.getDate();
		        }else{
		        	ri=d.getDate();
		        }
		        da=d.getFullYear() + "-" + yue + "-" + ri;
			   tim(da)
		        
		    }, function(e) {
		        //mui.toast("您没有选择日期");
		    }, {
		        title: '请选择日期',
		        date: dDate,
		        minDate: minDate,
		        maxDate: maxDate
		    });
		});
      	
		})

function tim(date){
	var aas="2017-04-13";
			$.post('oa.dazhi100.com/CloudOA/pubjs/icRecord_searchIcRecore',{date:date,openId:${openId}},function(data){
				console.log(data)
	      				if(data.ic==0){
	      					$("#den").css({"display":""});
	      				}else{
	      					$("#ji").css({"display":""});
	      					if(data.listIcardInfo.length==0){
	      						mui.toast("暂无打卡记录")
	      					}else{
	      						$("#tim").html(data.listIcardInfo[0].cdate.substring(0,10))
	      						var list=data.listIcardInfo;
	      						var span="";
	      						for(var s=0;s<list.length;s++){
	      							var jinchu;
									var img;
									if(list[s].type=="1"){
										jinchu="img/jin.png";
									}else if(list[s].type=="2"){
										jinchu="img/chu.png"
									}
									if(!list[s].img){
												img="";
											}else{
												img="img/tu.png"
											}
	      							span='<div style="padding:0px 10px!important;width: 100%;height:3rem;line-height:3rem;font-size: 1rem;background-color: #fff;border-bottom: 1px solid #d9d9d9;">'+
															'<span style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.2rem!important;background: url(img/fuka.png) 0rem 0rem no-repeat;background-size: 1.2rem 0.91rem;vertical-align: middle;"></span>'+
															'<span style="display: inline-block;height: 3rem;line-height: 3rem;text-indent: 0;width:50%;text-align:left;">'+list[s].icname+'</span>'+
															'<span style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.5rem!important;background: url('+jinchu+') 0rem 0rem no-repeat;background-size: 1.2rem 1.2rem;vertical-align: middle;"></span>'+
															'<span style="display: inline-block;height: 3rem;line-height: 3rem;text-indent: 0;margin: 0rem 1rem!important;">'+list[s].cdate.substring(11,19)+'</span>'+
															'<span class="datus" scs='+list[s].img+' style="display:inline-block;text-indent:0;width: 1.5rem!important;height: 1.5rem!important;background: url('+img+') 0rem 0rem no-repeat;background-size: 1.4rem 1.225rem;vertical-align: middle;"></span>'+
														'</div>'+span	
	      						}
	      						$("#yi").html(span);
	      						$("#yi").on("click",".datus",function(){
	      							layer.open({
										  type: 1,
										  skin: 'layui-layer-rim', //加上边框
										  area: ['100%', '100%'], //宽高
										  content:  '<img src='+$(this).attr("scs")+' width="100%" alt="" />'
										});
	      						})
	      					}
	      				}
	      			},'json')
}
	</script>
</html>
